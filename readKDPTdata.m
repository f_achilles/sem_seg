%% trying to parse Joaos Kinect data
close all
kinectPath = 'C:\Projects\ImageSegmentation\data\imageAnnotation\20131029171429969#6.kdpt';
depthFileHandle = fopen(kinectPath);
bitformat = '*uint16';
skipsize = 0;
skipBetweenFrames = 29;
tmp = fread(depthFileHandle,skipBetweenFrames,bitformat,skipsize);
tmp1 = fread(depthFileHandle,[640,480],bitformat,skipsize);

% as stated on
% https://social.msdn.microsoft.com/Forums/en-US/4da8c75e-9aad-4dc3-bd83-d77ab4cd2f82/common-nui-problems-and-faq?forum=kinectsdk
% , if player index is also retrieved, the 3 LSB will be this index, the
% following 12 will be the depth and the MSB is unused.
tmp1 = bitsra(tmp1, 3);
% tmp1 = bitand(tmp1, 2^12-1);
tmp1(tmp1>=2^12-1)=0;
% skip the timestamp (given in 8-bit character values, amount: 19)
tmp = fread(depthFileHandle,19,'uint8',skipsize);
tmp2 = fread(depthFileHandle,[640,480],bitformat,skipsize);
tmp2 = bitsra(tmp2, 3);
tmp2 = bitand(tmp2, 2^12-1);

tmp = fread(depthFileHandle,19,'uint8',skipsize);
% tmp = fread(depthFileHandle,skipBetweenFrames,bitformat,skipsize);
tmp3 = fread(depthFileHandle,[640,480],bitformat,skipsize);
tmp3 = bitsra(tmp3, 3);
tmp3 = bitand(tmp3, 2^12-1);

tmp = fread(depthFileHandle,19,'uint8',skipsize);
% tmp = fread(depthFileHandle,skipBetweenFrames,bitformat,skipsize);
tmp4 = fread(depthFileHandle,[640,480],bitformat,skipsize);
% depthFrame = fread(depthFileHandle,[640,480],bitformat,skipsize,'l');
% tmp = fread(depthFileHandle,[19],bitformat,skipsize,'l');
% tmp = fread(depthFileHandle,[640,480],bitformat,skipsize,'l');
% depthFrame = cat(2,depthFrame, fread(depthFileHandle,[640,480],bitformat,skipsize,'l'));
% depthFrame = round(255*depthFrame/max(max(depthFrame)));
depthFrame = cat(2, tmp1, tmp2, tmp3);
% depthFrame = tmp1;
figure
imagesc(depthFrame)
axis image
% depthFrame(depthFrame<1)=2^15;
% depthFrame=2^15-1-depthFrame;
% figure; surf(-depthFrame,'edgecolor','none');
% figure; hist(depthFrame(:),100);
% axis image

%% tests for KinectV2

close all
kinectPath = 'C:\Projects\ImageSegmentation\data\imageAnnotation\IM1225sz03\20141027013014516#2.kdpt';
depthFileHandle = fopen(kinectPath);
bitformat = '*uint16';
skipBetweenFrames = 40;
tmp = fread(depthFileHandle,skipBetweenFrames*2+1,'uint8');
% KV2 has 512�424 depth resolution
depthContainer = zeros(512,424,30*60,'uint16');
for i=1:30*60
tmp1 = fread(depthFileHandle,[512 424],bitformat);
depthContainer(:,:,i) = rot90(bitand(tmp1, 2^12-1),-1);
% tmp1 = bitsra(tmp1, 4);
tmp = fread(depthFileHandle,19,'uint8');
end
% tmp2 = fread(depthFileHandle,[512 424],bitformat);
% tmp2 = bitand(tmp2, 2^12-1);
% tmp = fread(depthFileHandle,19,'uint8');
% tmp3 = fread(depthFileHandle,[512 424],bitformat);
% tmp3 = bitand(tmp3, 2^12-1);
% depthFrame = cat(2,tmp1,tmp2,tmp3);
% depthFrame = tmp1;
imagesc(depthContainer(:,:,i-1))
axis image
