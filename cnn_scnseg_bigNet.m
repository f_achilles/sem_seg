function [net, info] = cnn_scnseg_bigNet(imdb)
% cnn_scnseg
% Test architecture for semantic segmentation on NYU2 dataset

% version7:
% Fully and Conv trained together from the beginning
% Fully has 100times higher learning rate
% Validation-set consists of (random) 10% of the augmented data

% version bigNet:
% same as version7 but:
% reLU after eacht conv layer
% upscale layer compares to full-res-labels (240x320)

randomIndices = randperm(numel(imdb.images.set));

imdb.images.set(randomIndices(1:(ceil(numel(randomIndices)/10)))) = 2;

opts.dataDir = fullfile('..\data') ;
opts.expDir = fullfile('..\data\depthNet\bigNet') ;
opts.imdbPath = fullfile(opts.expDir, 'NYU2scale10TrainingAllLabels4ClassesExp.mat');
opts.train.batchSize = 100 ;
opts.train.numEpochs = 100 ;
opts.train.continue = true ;
opts.train.useGpu = true ;
opts.train.learningRate = 0.001 ;
opts.train.expDir = opts.expDir ;
opts.continue = true;
opts.train.sync = false;
% opts = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

if exist(opts.imdbPath, 'file') && ~exist('imdb','var')
    imdb = load(opts.imdbPath) ;
    % convert struct data to 4D matrices format
    imdb = convertTo4dStruct(imdb.imdb);
elseif ~exist('imdb','var')
    error('No training data found!')
end

% Define a network similar to Fergus&Eigen
f=1/100 ;

% load network trained without fully connected layer ("pretrained")
% load('C:\Projects\ImageSegmentation\data\depthNet\version3\net-epoch-97.mat')

% COARSE SCALE part
net.layers = {} ;
% input size 240x320
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(7,7,1,16, 'single'), ...
                           'biases', zeros(1, 16, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
% H'' = 240-7+1=234; W'' = 320-7+1=314;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;
% H'' = 234/2 = 117; W'' = 314/2 = 157;
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(6,6,16,64, 'single'),...
                           'biases', zeros(1,64,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
% H'' = 117-6+1=112; W'' = 157-6+1=152;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;
% H'' = 112/2 = 56; W'' = 152/2 = 76;
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(5,5,64,16, 'single'),...
                           'biases', zeros(1,16,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
% H'' = 56-5+1=52; W'' = 76-5+1=72;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;
% H'' = 52/2 = 26; W'' = 72/2 = 36;
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(3,5,16,4, 'single'),...
                           'biases', zeros(1,4,'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
% H'' = 26-3+1 = 24; W'' = 36-3+1 = 34;
%%%
% fully connected layer 1
%%%
% replace softmaxloss by FC layer
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(24,32,4,1024, 'single'),...
                           'biases', zeros(1,1024,'single'), ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 100, ...
                           'biasesLearningRate', 100) ;
net.layers{end+1} = struct('type', 'relu') ;
%%%
% fully connected layer 2
%%%
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,1024,24*32*4, 'single'),...
                           'biases', zeros(1,24*32*4,'single'), ...
                           'stride', 1, ...
                           'pad', 0,...
                           'filtersLearningRate', 100, ...
                           'biasesLearningRate', 100) ;
net.layers{end+1} = struct('type', 'relu') ;
%%%
% reshape layer
%%%
net.layers{end+1} = struct('type', 'reshape', ...
                           'param', [24*32*4 24 32 4]) ;
%%%
% upscale layer
%%%
net.layers{end+1} = struct('type', 'upscale', ...
                           'param', 10) ;
net.layers{end+1} = struct('type', 'softmaxloss') ;


% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

% Take the mean out
imdb.images.data = bsxfun(@minus, imdb.images.data, mean(imdb.images.data(:,:,:,imdb.images.set==1),4)) ;

[net, info] = cnn_train_scnseg(net, imdb, @getBatch, ...
    opts.train) ;

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(:,:,1,batch) ;
