function imdb = convertTo4dStruct(databaseStruct)
% convertTo4dImage converts a struct with training/testing data into the 4D
% struct format required by MatConvNet.
%
% input:
% output:

N = numel(databaseStruct);
H = size(databaseStruct(1).im,1);
W = size(databaseStruct(1).im,2);
numChannels      = size(databaseStruct(1).im,3);
imdb.images.data = reshape([databaseStruct.im],H,W,numChannels,N);

imdb.images.set    = [databaseStruct.set];

Hout = size(databaseStruct(1).y,1);
Wout = size(databaseStruct(1).y,2);
imdb.images.labels = reshape([databaseStruct.y],Hout,Wout,1,N);

end