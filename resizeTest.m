majorityFilterKernel = @(x)majorityFilter(x,0:4);

majBlockFun = @(blockStr)majorityFilter(blockStr.data,0:4);
majDownScale = blockproc(labels(:,:,1),[20 20],majBlockFun);



for i=1:1449
% firstRescaleThenRotate = imrotate(imresize(labels(:,:,i),[24,32],'nearest'),8,'nearest','crop');
% zeroPixelsFirstResc(i) = sum(firstRescaleThenRotate(:)==0);

% firstRotateThenResacle = imresize(imrotate(labels(:,:,i),8,'nearest','crop'),[24,32],'nearest');
% zeroPixelsFirstRot(i) = sum(firstRotateThenResacle(:)==0);

B = nlfilter(labels(:,:,i),[5 5],majorityFilterKernel);
figure; imagesc(B); axis image

% firstMajorResc=imresizeNonLin(labels(:,:,i),[24,32],'method',{majorityFilterKernel,13},'Antialiasing',false);
% firstMedianRescThenRot = imrotate(imresizeNonLin(labels(:,:,i),[24,32],'method',{majorityFilterKernel,5},'Antialiasing',false),8,'nearest','crop');
% zeroPixelsFirstMedianResc(i) = sum(firstMedianRescThenRot(:)==0);
end

figure
subplot(1,3,1)
plot(sort(zeroPixelsFirstResc))
title('Rescale first')
subplot(1,3,2)
plot(sort(zeroPixelsFirstRot))
title('Rotate first')
subplot(1,3,3)
plot(sort(zeroPixelsFirstMedianResc))
title('Median Rescale first')