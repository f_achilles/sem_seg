function Y = vl_f_upscale(X,params,dzdy)
%     persistent interpD1 interpD2 sz
    % X is element of [sz(1),sz(2),sz(3),batchsize]
    % params is the forward-scale
    if nargin <= 2
%         if isempty(interpD1)
        sz = size(X);
        interpD1 = linspace(1,sz(1),params(1)*sz(1));
        interpD2 = (linspace(1,sz(2),params(1)*sz(2)))';
%         end
        if numel(sz) > 3
            Y = interpn(X,interpD1,interpD2,1:sz(3),1:sz(4),'linear');
        else
            Y = interpn(X,interpD1,interpD2,1:sz(3),'linear');
        end
    else
        Y = vl_nnpool(dzdy,params(1),'stride',params(1),'method','avg');
    end
end