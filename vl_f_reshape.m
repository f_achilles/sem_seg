function Y = vl_f_reshape(X,params,dzdy)
    % X is element of [1,1,params(1),batchsize]
    if params(1)~=params(2)*params(3)*params(4)
        error('reshape size does not match')
    end
    if nargin <= 2
        Y = reshape(X,[params(2) params(3) params(4) size(X,4)]);
    else
        Y = reshape(dzdy,[1 1 params(1) size(X,4)]);
    end
end