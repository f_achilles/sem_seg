%% Evaluate net by visualization of filters / feature maps

% visualize filters (first layer)
figure
firstFilters=net.layers{2}.filters;
colormap(gray)
for i=1:16, subplot(4,4,i);imagesc(firstFilters(:,:,2,i)); title(['Filter ' num2str(i)]);axis image off; end


figure
ZMIN = gather(min(firstFilters(:)));
ZMAX = gather(max(firstFilters(:)));
for i=1:16, subplot(4,4,i);surf(double(firstFilters(:,:,1,i)),'edgecolor','none','facecolor','interp'); zlim([ZMIN ZMAX]); axis vis3d; view(3); title(['Filter ' num2str(i)]); end

% visualize 3rd filter layer (second convolution)
figure
secondFilters=net.layers{3}.filters;
for i=1:64, subplot(8,8,i);imagesc(secondFilters(:,:,1,i)); title(['Filter ' num2str(i)]);axis image off; end

% visualize 2nd feature map layer
% obtain and preprocess an image
im = imdb.images.data(:,:,1,1)-net.meta.meanImage;
figure
imagesc(im); axis image off
title('input image')
% run the CNN
net.layers{end}.type = 'softmax';
res = vl_simplenn_scnseg(net, gpuArray(im)) ;
featureMapL2 = res(6).x;
figure
colormap(gray) %hot
for i=1:16, subplot(4,4,i);imagesc(featureMapL2(:,:,i)); title(['Filter ' num2str(i)]);axis image off; end

% visualize 4th feature map layer
featureMapL4 = res(14).x;
figure
colormap(jet) %hot
for i=1:128, subplot(8,16,i);imagesc(featureMapL4(:,:,i)); title(['Filter ' num2str(i)]);axis image off; end

% visualize 6th feature map layer
featureMapL6 = res(6).x;
figure
colormap(jet) %hot
for i=1:16, subplot(4,4,i);imagesc(featureMapL6(:,:,i)); title(['Filter ' num2str(i)]);axis image off; end

% visualize 8th feature map layer
featureMapL8 = res(8).x;
figure
colormap(jet) %hot
for i=1:4, subplot(2,2,i);imagesc(featureMapL8(:,:,i)); title(['Filter ' num2str(i)]);axis image off; end

% visualize 10th feature map layer
featureMapL10 = res(10).x;
figure
colormap(jet) %hot
for i=1:4, subplot(2,2,i);imagesc(featureMapL10(:,:,i)); title(['Filter ' num2str(i)]);axis image off; end

%% Evaluate on some random images
overlapMap = jet(5);
% imdb=imdbVal;
% run net on random training images and plot output
ridx=randi(size(imdb.images.data(:,:,:,imdb.images.set==1),4),1,3);
trainingIndices = find(imdb.images.set==1);
ridx = trainingIndices(ridx);
results = cell(1,3);
% exchange last softmaxloss-layer for softmax (forward only)
net.layers{end}.type = 'softmax';
for i=1:3
% obtain and preprocess an image
im = imdb.images.data(:,:,1,ridx(i));
% run the CNN
results{i} = vl_simplenn_scnseg(net, gpuArray(im-imdb.meta.meanImage)) ;
end
% show the classification result
figure
for i=1:3
histogramOutput=results{i}(end).x;
[~,classRes]=max(histogramOutput,[],3);
% classRes=classRes-1;
% calculate pixel accuracy
pixelAcc = sum(sum(classRes==imdb.images.labels(:,:,1,ridx(i)) & imdb.images.labels(:,:,1,ridx(i))~=0))/sum(sum(imdb.images.labels(:,:,1,ridx(i))~=0));
% output plot
subplot(4,3,i);imagesc(classRes); axis image; caxis([0 4])
title({['output ' num2str(i) ': '], [ num2str(100*pixelAcc) '% pixelwise accuracy']})
% GT plot
subplot(4,3,i+3);imagesc(imdb.images.labels(:,:,1,ridx(i))); axis image; caxis([0 4])
title(['ground truth ' num2str(i)])
% input plot
subplot(4,3,i+6);imagesc(imdb.images.data(:,:,1,ridx(i))-imdb.meta.meanImage); axis image
title(['input ' num2str(i)])
% input overlay with labels
croppedResult = classRes;%classRes(:,2:(end-1));
% subplot(4,3,i+9);imagesc(bsxfun(@times,imdb.images.data(:,:,1,ridx(i)), imresize(reshape(overlapMap(croppedResult(:)+1,:), [size(croppedResult) 3]),10, 'bilinear')) / (max(max(imdb.images.data(:,:,1,ridx(i)))))); axis image
subplot(4,3,i+9);imagesc(bsxfun(@times,imdb.images.data(:,:,1,ridx(i)), reshape(overlapMap(croppedResult(:)+1,:), [size(croppedResult) 3])) / (max(max(imdb.images.data(:,:,1,ridx(i)))))); axis image
title(['input with overlay ' num2str(i)])
end

%% Evaluate on the validation set
load('..\data\betterLabels\NYU2scale10Validation.mat')
N = size(imdbVal.images.data,4);
pixelAcc = zeros(1,N);
t = zeros(1,N);
net.layers{end}.type = 'softmax';
for i=1:N
    im = imdbVal.images.data(:,:,1,i);
    tic
    res=vl_simplenn(net, gpuArray(im));
    softmaxOut = gather(res(end).x);
    t(i)=toc;
    [~,classRes]=max(softmaxOut,[],3);
%     classRes=classRes-1;
    % calculate pixel accuracy
    pixelAcc(i) = sum(sum(classRes==imdbVal.images.labels(:,:,1,i) & imdbVal.images.labels(:,:,1,i)~=0))/sum(sum(imdbVal.images.labels(:,:,1,i)~=0));
end
disp(['Mean pixel accuracy: ' num2str(100*mean(pixelAcc)) '%.'])
disp(['Mean time per frame: ' num2str(mean(t)) ' seconds.'])

figure;
plot(sort(pixelAcc)); axis tight

[sortedAcc, sortedIndices] = sort(pixelAcc);
overlapMap = jet(5);
[maxval,maxind]=max(pixelAcc);
% maxind = gather(sortedIndices(end-3));
im = imdbVal.images.data(:,:,1,maxind);
res=vl_simplenn(net, gpuArray(im));
softmaxOut = gather(res(end).x);
[~,classRes]=max(softmaxOut,[],3);
% classRes=classRes-1;
croppedResult = classRes;
figure; imagesc(bsxfun(@times,imdbVal.images.data(:,:,1,maxind), imresize(reshape(overlapMap(croppedResult(:)+1,:), [size(croppedResult) 3]),10, 'bilinear')) / (max(max(imdbVal.images.data(:,:,1,maxind))))); axis image
figure; imagesc(classRes); axis image; caxis([0 4])
figure; imagesc(imdbVal.images.labels(:,:,1,maxind)); axis image; caxis([0 4])


%% Generate test set results
N = nnz(imdb.images.set == 2);
testResults = cell(1,N);
testInds = find(imdb.images.set == 2);
pixelAcc = zeros(1,N);
t = zeros(1,N);
net.layers{end}.type = 'softmax';
for i=1:N
    im = imdb.images.data(:,:,1,testInds(i));
    tic
    res=vl_simplenn_scnseg(net, gpuArray(im-imdb.meta.meanImage));
    softmaxOut = gather(res(end).x);
    t(i)=toc;
    testResults{i}=softmaxOut;
    [~,classRes]=max(softmaxOut,[],3);
    % calculate pixel accuracy
    pixelAcc(i) = sum(sum(classRes==imdb.images.labels(:,:,1,testInds(i)) & imdb.images.labels(:,:,1,testInds(i))~=0))/sum(sum(imdb.images.labels(:,:,1,testInds(i))~=0));
end
disp(['Mean pixel accuracy: ' num2str(100*mean(pixelAcc)) '%.'])
disp(['Mean time per frame: ' num2str(mean(t)) ' seconds.'])
% store ground truth labels for each test frame
testGroundTruths = cell(1,N);
for i=1:N
    testGroundTruths{i} = squeeze(imdb.images.labels(:,:,:,testInds(i)));
end

save('labellingVolumes','testResults','testGroundTruths','-v7.3');




