%% script for extracting seizures from KDPT data

filename = input('Type in the filename: ');

[depthContainer, timestampContainer] = playKDPT(filename);

filename2 = input('Want to append another file? Just press Enter if not: ');

if ~isempty(filename2)
    [depthContainer2, timestampContainer2] = playKDPT(filename2);
    depthContainer = cat(3,depthContainer,depthContainer2);
    timestampContainer = cat(2,timestampContainer,timestampContainer2);
    clear depthContainer2 timestampContainer2
end
frameLow = input('type START frame number (combined) and press enter (just press enter if it is frame 1): ');
if isempty(frameLow)
    frameLow = 1;
end

frameHigh = input('type END frame number (combined) and press enter (just press enter if it is the last frame): ');
if isempty(frameHigh)
    frameHigh = size(depthContainer,3);
end

depthContainer = depthContainer(:,:,frameLow:frameHigh);

% depthSeizurePart = gpuArray(depthContainer(:,:,frameLow:frameHigh));
% 
% 
% %% now compare the frames and find a set of 1/10th of the frames that are maximally different from one another
% % 1/10th of the seizure is too much! it might have 1000 frames, so we get
% % 100 images, many of which are very similar.
% %
% % a fixed number per seizure is maybe better. If we have 34 seizures (we
% % do), and we need about 1200 labelled images (we do!), then 40 frames per
% % seizure should be good.
% 
% outputsize = 40;
% offs = ceil(size(depthSeizurePart,3)/(2*outputsize));
% indexvec = ones(size(depthSeizurePart,3),1);
% % outputContainer = zeros(size(depthSeizurePart,1),size(depthSeizurePart,2),outputsize);
% currentFrameInd = gpuArray(1);
% seizurePartFrameIndices = ones(1,outputsize);
% for outInd = 1:outputsize
%     seizurePartFrameIndices(outInd) = gather(currentFrameInd);
%     indexvec((max(currentFrameInd-offs,1)):min(currentFrameInd+offs,size(depthSeizurePart,3)))=0;
%     diffs = bsxfun(@minus,depthSeizurePart,depthSeizurePart(:,:,currentFrameInd));
%     diffsvec = squeeze(sum(sum(diffs.^2)));
%     [~,currentFrameInd] = max(diffsvec.*indexvec);
%     disp(['frame ' num2str(outInd) ' computed.'])
% end
% seizurePartFrameIndices = sort(seizurePartFrameIndices);
% outputContainer = gather(depthSeizurePart(:,:,seizurePartFrameIndices));
% 
% % store corresponding frame numbers
% seizurePart = frameLow:frameHigh;
% originalFrameNumbers = seizurePart(seizurePartFrameIndices);
% timestamps = timestampContainer(seizurePart(seizurePartFrameIndices));
% 
% [seizurePath,seizureFilename, extension] = fileparts(filename);
% splitpath = regexp(seizurePath,filesep,'split');
% save([seizurePath filesep 'labelImages_' splitpath{end} '_' extension(2:end) '_' seizureFilename '.mat'], 'outputContainer', 'timestamps', 'originalFrameNumbers', '-v7.3');

randomSelectionScheme

[seizurePath,seizureFilename, extension] = fileparts(filename);
if ~isempty(filename2)
    [~,seizureFilename2] = fileparts(filename2);
    seizureFilename = [seizureFilename seizureFilename2];
end
splitpath = regexp(seizurePath,filesep,'split');
save([seizurePath filesep 'labelImages_' splitpath{end} '_' extension(2:end) '_' seizureFilename '.mat'], 'outputContainer', 'timestamps', 'originalFrameNumbers', '-v7.3');

% export as .png using imwrite
grayHarmonicMap = repmat(gray(750),10,1);
grayHarmonicVec = grayHarmonicMap(:,1);
for frameInd = 1:outputsize
    im = outputContainer(:,:,frameInd);
    im(im==0)=1;
    indexedImg = reshape(grayHarmonicVec(im(:)),size(im));
    imshow(indexedImg);
    title(['saving ' extension(2:end) 'Frame' num2str(frameInd) '.png ...'])
    drawnow
    imwrite(indexedImg,[seizurePath filesep extension(2:end) 'Frame' num2str(frameInd) '.png']);
end

