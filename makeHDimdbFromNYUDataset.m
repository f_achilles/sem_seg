currFName = which('augmentNYUDatasetMultiscaleGT');
currPath = fileparts(currFName);

load([currPath '\..\data\nyu_depth_v2_labeled.mat'],'depths')
load([currPath '\..\data\NYU2labels4classes.mat'])

N = size(labels,3);
hOriginal = size(labels,1);
wOriginal = size(labels,2);

hIn = hOriginal/2;
wIn = wOriginal/2;

depths2 = zeros(hIn,wIn,N,'single');
labels2 = zeros(hIn,wIn,N,'uint16');
for i = 1:N
depths2(:,:,i) = imresize(depths(:,:,i),[hIn,wIn],'bilinear');
labels2(:,:,i) = imresize(labels(:,:,i),[hIn,wIn],'nearest');
end
imdb.images.data = permute(depths2,[1 2 4 3]);
imdb.images.labels = permute(labels2,[1 2 4 3]);

% split into 795 training and 645 test images
% indices for tran/test split are taken from
% http://horatio.cs.nyu.edu/mit/silberman/indoor_seg_sup/splits.mat
load([currPath '\..\NYU2TrainTestSplits.mat'])
trainingSize   = numel(trainNdxs);
validationSize = numel(testNdxs);

imdb.images.set = ones(1,trainingSize+validationSize);
imdb.images.set(testNdxs) = 2;