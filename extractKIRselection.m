% script for extracting seizures from KIR data

filename = input('Type in the filename: ');

[infraredContainer, timestampContainer] = playKIR(filename);

filename2 = input('Want to append another file? Just press Enter if not: ');

if ~isempty(filename2)
    [infraredContainer2, timestampContainer2] = playKIR(filename2);
    infraredContainer = cat(3,infraredContainer,infraredContainer2);
    timestampContainer = cat(2,timestampContainer,timestampContainer2);
    clear depthContainer2 timestampContainer2
end
frameLow = input('type START frame number (combined) and press enter (just press enter if it is frame 1): ');
if isempty(frameLow)
    frameLow = 1;
end

frameHigh = input('type END frame number (combined) and press enter (just press enter if it is the last frame): ');
if isempty(frameHigh)
    frameHigh = size(infraredContainer,3);
end

[seizurePath,seizureFilename, extension] = fileparts(filename);
if ~isempty(filename2)
    [~,seizureFilename2] = fileparts(filename2);
    seizureFilename = [seizureFilename seizureFilename2];
end
splitpath = regexp(seizurePath,filesep,'split');

% look for stored .mat file

matFile = dir([seizurePath filesep '*' seizureFilename '.mat']);
if isempty(matFile)
    error('.mat file with selected timestamps and frame numbers not found!')
end

load([seizurePath filesep matFile.name])

N=numel(timestamps);
outputIR = outputContainer*0;
for tIdx = 1:N
    frameIdx = find(timestampContainer==timestamps(tIdx));
    if ~isempty(frameIdx)
        outputIR(:,:,tIdx) = infraredContainer(:,:,frameIdx);
        if frameIdx == originalFrameNumbers(tIdx)
            disp 'Hooray!'
        end
    else
        disp 'Duh!'
    end
end

save([seizurePath filesep 'labelImages_' splitpath{end} '_' extension(2:end) '_' seizureFilename '.mat'], 'outputIR', 'timestamps', 'originalFrameNumbers', '-v7.3');

% export as .png using imwrite
for frameInd = 1:N
    im = outputIR(:,:,frameInd);

    indexedImg = histeq(im, 1000);
    imshow(indexedImg);
    title(['saving ' extension(2:end) 'FrameIR' num2str(frameInd) '.png ...'])
    drawnow

    imwrite(indexedImg,[seizurePath filesep extension(2:end) 'Frame' num2str(frameInd) '.png']);
end
