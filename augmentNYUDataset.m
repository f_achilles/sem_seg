% split images into 2x3 grids of 200x200 pixels each
% this results in 1449*6=8694 images
clc
clear all
load('C:\Users\Felix\Downloads\nyu_depth_v2_labeled.mat')

imdb = struct('im',[],'y',[],'set',[],'fr');
% im = image patch as H x W x numColors
% y  = label patch as H x W x 1
% set = element of [1;2]
% fr = index of the origin frame of patch 'im'

inputEdgeLength = 200; %pixels of the cube-patch edgelength
N = size(rawDepths,3);
hOriginal = size(rawDepths,1);
wOriginal = size(rawDepths,2);
numGridRows = floor(hOriginal/inputEdgeLength);
numGridCols = floor(wOriginal/inputEdgeLength);

NYUGridPatchesDepth = zeros([inputEdgeLength,inputEdgeLength,N*numGridRows*numGridCols],'single');
NYUGridPatchesLabels = zeros([inputEdgeLength,inputEdgeLength,N*numGridRows*numGridCols],'uint16');

for frameInd = 1:N
    for rowInd = 1:numGridRows
        for colInd = 1:numGridCols
            cInd = sub2ind([numGridRows,numGridCols,N],rowInd,colInd,frameInd);
            NYUGridPatchesDepth(:,:,cInd) = depths(((rowInd-1)*inputEdgeLength+1):(rowInd*inputEdgeLength),((colInd-1)*inputEdgeLength+1):(colInd*inputEdgeLength),frameInd);
            NYUGridPatchesLabels(:,:,cInd) = labels(((rowInd-1)*inputEdgeLength+1):(rowInd*inputEdgeLength),((colInd-1)*inputEdgeLength+1):(colInd*inputEdgeLength),frameInd);
        end
    end
end

clear depths rawDepths labels images instances

NYUGridPatchesDepth = reshape(NYUGridPatchesDepth,[inputEdgeLength inputEdgeLength 1 N*numGridRows*numGridCols]);
save('NYU2DepthsPatches0rot','NYUGridPatchesDepth')
save('NYU2LabelsPatches0rot','NYUGridPatchesLabels')

disp('You made it! Dataset stored on disk, RAM can breathe again.')

% rotate gridded images about +-3,6,9 degrees
% this results in 8694*7=60858 images

NYUGridPatchesDepthRotated = zeros([inputEdgeLength,inputEdgeLength,1,N*numGridRows*numGridCols],'single');
NYUGridPatchesLabelsRotated = zeros([inputEdgeLength,inputEdgeLength,N*numGridRows*numGridCols],'uint16');

for rotAngle = [-9 -6 -3 3 6 9]
    for patchInd = 1:N*numGridRows*numGridCols
        NYUGridPatchesDepthRotated(:,:,1,patchInd)=imrotate(NYUGridPatchesDepth(:,:,1,patchInd),rotAngle,'bilinear','crop');
        NYUGridPatchesLabelsRotated(:,:,patchInd)=imrotate(NYUGridPatchesLabels(:,:,patchInd),rotAngle,'nearest','crop'); %nearestneighbor for labels, as they cannot be interpolated
    end
    save(['NYU2DepthsPatches' num2str(rotAngle) 'rot'],'NYUGridPatchesDepthRotated')
    save(['NYU2LabelsPatches' num2str(rotAngle) 'rot'],'NYUGridPatchesLabelsRotated')
    
    disp(['NYU2DepthsPatches' num2str(rotAngle) 'rot and NYU2LabelsPatches' num2str(rotAngle) 'rot were stored to disk.'])
end

clear all
disp('You made it! Dataset stored on disk, RAM can breathe again.')

% disp('Paused. Press any key to continue...')
% pause