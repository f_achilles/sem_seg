function varargout = playKIR(filename,verbose,rotate)
if ~exist('verbose','var') || isempty(verbose)
    verbose = false;
end
if ~exist('rotate','var') || isempty(rotate)
    rotate = false;
end
output = 0;
if nargout > 0
    infraredContainer = zeros(424,512,30*60,'uint16');
    timestampContainer = zeros(1,30*60);
    output = 1;
end
infraredFileHandle = fopen(filename);
bitformat = '*uint16';

mainHeader=textscan(infraredFileHandle,'%s\n',2);
disp(mainHeader{1})

% read initial frame
frameNr = 1;
timestamp = fread(infraredFileHandle,18,'uint8');
disp(['time: ' char(timestamp(1:end-1)') ', frame: ' num2str(frameNr)])
% KV2 has 512�424 infrared resolution
tmp1 = fread(infraredFileHandle,[512 424],bitformat);
tmp1 = rot90(tmp1,-1);
if output
    infraredContainer(:,:,1) = tmp1;
    timestampContainer(1) = str2double(char(timestamp(1:end-1)'));
end
if verbose
colormap hot
hD=imagesc(tmp1,[0 2^12-1]);
axis image
title('KinectV2 infrared video')
hT=text(10,40,{['time: ' char(timestamp(1:end-1)') ','],['frame: ' num2str(frameNr)]},'fontsize',20);
end

% read all frames until EOF and update figure
while 1  
timestamp = fread(infraredFileHandle,19,'uint8');
if isempty(timestamp)
    break
end
frameNr = frameNr+1;
disp(['time: ' char(timestamp(2:end-1)') ', frame: ' num2str(frameNr)])
% KV2 has 512�424 infrared resolution
tmp1 = fread(infraredFileHandle,[512 424],bitformat);
tmp1 = rot90(tmp1,-1);
if verbose
set(hD,'CDATA',tmp1);
end
if output
    infraredContainer(:,:,frameNr) = tmp1;
    timestampContainer(frameNr) = str2double(char(timestamp(2:end-1)'));
end
if verbose
set(hT,'string',{['time: ' char(timestamp(2:end-1)') ','],['frame: ' num2str(frameNr)]});
drawnow;
end

end
fclose(infraredFileHandle);

if output
    infraredContainer   = infraredContainer(:,:,1:frameNr);
    timestampContainer  = timestampContainer(1:frameNr);
    varargout{1}    = infraredContainer;
    varargout{2}    = timestampContainer;
end

end