function [patch, pose, tf] = getImPatch(img,x,y,wi,hei,poseGT)

%crop
crp=img(y:y+hei,x:x+wi,:);

%resize
patchSiz_x=320;
patchSiz_y=240;
%crp=imresize(crp,[patchSiz,patchSiz]);
scale_fa_x=wi/patchSiz_x;
scale_fa_y=hei/patchSiz_y;

%GT points
poseGTresc=poseGT(:,1:2);
poseGTresc(:,1)=poseGTresc(:,1)-x;
poseGTresc(:,2)=poseGTresc(:,2)-y;
%poseGTresc(:,1)=poseGTresc(:,1)./scale_fa_x;
%poseGTresc(:,2)=poseGTresc(:,2)./scale_fa_y;
%poseGTresc = reshape (poseGTresc',size(poseGTresc,1)*2,1);

%patch=crp;
%pose=poseGTresc;

tf=[1/scale_fa_x 0 0; 0 1/scale_fa_y 0; 0 0 1];
tform = maketform('affine',tf);
[patch, grn, gcn] = transformImage(crp, poseGTresc(:,2), poseGTresc(:,1), tform);
%crop once more being sure about the size of the image
patch=patch(1:patchSiz,1:patchSiz,:);
poseGTresc(:,2)=grn;
poseGTresc(:,1)=gcn;
pose=poseGTresc;


end