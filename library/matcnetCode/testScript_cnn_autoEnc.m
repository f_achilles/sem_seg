%% script for autoencoder testing
% train
uiopen('C:\Users\Felix\Downloads\edit_63_motto_04.jpg',1)
resizeEdgeLength = 410;
testImg = edit_63_motto_04(300+(1:resizeEdgeLength),300+(1:resizeEdgeLength),:);

imdb.images.set = 1;
imdb.images.labels = single(testImg);
imdb.images.data = single(testImg);

[net, info] = cnn_autoEnc(imdb);

% test
net2=net;
net2.layers = net2.layers(1:end-1);
res=vl_simplenn_scnseg(net2,gpuArray(single(testImg)));

close all
for iFmap = 1:numel(res)
    figure
    fMap = single(res(iFmap).x);
    sz_ = size(fMap);
    if sz_(3) == 3
    minVal = abs(min(reshape(fMap,1,[])));
    fMap = fMap+minVal;
    maxVal = max(reshape(fMap,1,[]));
    fMap = fMap/maxVal;
    imshow(fMap)
    else
        for iCh = 1:16%sz_(3)
            subplot(4,4,iCh)
            imagesc(fMap(:,:,iCh)); axis image
            title(['fMap ' num2str(iCh)])
        end
    end
end
