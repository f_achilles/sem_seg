function [Y, Y2] = vl_fmMultiply(X1, X2, dzdy)
% VL_FMMULTIPLY     Takes two feature maps of equal size and multiplies
% them element-wise.
assert(all(size(X1)==size(X2)),'Feature map sizes are not equal!')
if nargin < 3
    Y = X1.*X2;
else
    Y  = X2.*dzdy;
    Y2 = X1.*dzdy;
end
end