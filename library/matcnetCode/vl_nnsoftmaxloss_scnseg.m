function Y = vl_nnsoftmaxloss_scnseg(X,c,dzdy)
% VL_NNSOFTMAXLOSS  CNN combined softmax and logistic loss
%    Y = VL_NNSOFTMAX(X, C) applies the softmax operator followed by
%    the logistic loss the data X. X has dimension H x W x D x N,
%    packing N arrays of W x H D-dimensional vectors.
%
%    C contains the class labels, which should be integer in the range
%    1 to D.  C can be an array with either N elements or with H x W x
%    1 x N dimensions. In the fist case, a given class label is
%    applied at all spatial locations; in the second case, different
%    class labels can be specified for different locations.
%
%    D can be thought of as the number of possible classes and the
%    function computes the softmax along the D dimension. Often W=H=1,
%    but this is not a requirement, as the operator is applied
%    convolutionally at all spatial locations.
%
%    DZDX = VL_NNSOFTMAXLOSS(X, C, DZDY) computes the derivative DZDX
%    of the CNN with respect to the input X given the derivative DZDY
%    with respect to the block output Y. DZDX has the same dimension
%    as X.

% Copyright (C) 2014 Andrea Vedaldi.
% All rights reserved.
%
% This file is part of the VLFeat library and is made available under
% the terms of the BSD license (see the COPYING file).

%X = X + 1e-6 ;
sz = [size(X,1) size(X,2) size(X,3) size(X,4)] ;

% index from 0
c = c - 1 ;

if numel(c) == sz(4)
  % one label per image
  c = reshape(c, [1 1 1 sz(4)]) ;
  c = repmat(c, [sz(1) sz(2)]) ;
else
  % one label per spatial location
  aid = ones(1,4);
  sz_ = size(c) ;
  aid(1:numel(sz_)) = sz_;
  sz_ = aid;
  try
  assert(isequal(sz_, [sz(1) sz(2) 1 sz(4)])) ;
  catch
  disp 'darn'    
  end
end

% convert to indeces
c_ = 0:numel(c)-1 ;
c_ = 1 + ...
  mod(c_, sz(1)*sz(2)) + ...
  (sz(1)*sz(2)) * c(:)' + ...
  (sz(1)*sz(2)*sz(3)) * floor(c_/(sz(1)*sz(2))) ;
% some calculations helping with unlabelled images/pixels
X_dummy = gpuArray(c_*0);
c_validBinary = c(:)>=0;
X_dummy(c_validBinary) = X(c_(c_validBinary));
% compute softmaxloss
Xmax = max(X,[],3) ;
ex = exp(bsxfun(@minus, X, Xmax)) ;

n = sz(1)*sz(2) ;
if nargin <= 2
  t = Xmax + log(sum(ex,3)) - reshape(X_dummy, [sz(1:2) 1 sz(4)]) ;
  Y = sum(t(c_validBinary)) / n ;
else
  Y = bsxfun(@rdivide, ex, sum(ex,3)) ;
  uniquePositiveIndices = unique(c_(c_validBinary));
  Y(uniquePositiveIndices) = Y(uniquePositiveIndices) - 1;
  %   Y(c_(c_validBinary)) = Y(c_(c_validBinary)) - 1;
  % unlabelled images/pixels do not influence the loss, hence their
  % derivative is 0 in all channels
  Y=bsxfun(@times,Y,c>=0);
  %   [indH,indW,indInst]=ind2sub([sz(1) sz(2) sz(4)], find(~c_validBinary));
  %   Y(indH,indW,:,indInst) = 0;
  Y = Y * (dzdy / n);
  % doubling the importance of event 1 (seizure!)
  %   Y = bsxfun(@times, Y * (dzdy / n), [4;1]/5);
%   Y = bsxfun(@times, Y, single(reshape([0.0150523508544095 1.06606355114964 1.17250636448796 1.21121776428937 1.49306220220838 1.83660377865849 0.829420676927374 0.523511913598746 0.330910440606569 0.817179589134476 1.66645166827602 1.10025944512459 0.367316591773961 1.10518295039603 0.887008136970801 0.139713354581346 1.63781446704620 1.71646143244864 0.897115247169143 0.773355443952199 1.41379263034567],1,1,[])));
end
