function [net, info] = cnn_autoEnc(imdb)

opts.dataDir = fullfile('C:\Projects\SemanticSegmentation\data\testNetForDeconv') ;
opts.expDir = fullfile('C:\Projects\SemanticSegmentation\data\autoEncodeNet') ;
opts.imdbPath = fullfile(opts.expDir, 'NYU2scale10TrainingAllLabels4ClassesExp.mat');
opts.train.batchSize = 50 ;
opts.train.numEpochs = 250 ;
opts.train.continue = true ;
opts.train.useGpu = true ;
opts.train.learningRate = 0.00001 ;
opts.train.expDir = opts.expDir ;
opts.continue = true;
opts.train.sync = false;
% opts = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------


% Define a network similar to Fabaret
f=1/100 ;

% load network trained without fully connected layer ("pretrained")
% load('C:\Projects\ImageSegmentation\data\depthNet\version3\net-epoch-97.mat')

net.layers = {} ;
%410
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(7,7,3,16, 'single'), ...
                           'biases', zeros(1, 16, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
%404
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;
%202
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(3,3,16,64, 'single'), ...
                           'biases', zeros(1, 64, 'single'), ...
                           'stride', 2, ...
                           'pad', 0) ;
% two-fold subsampled with a multitude of layers (21-fold)
%100
net.layers{end+1} = struct('type', 'deconv', ...
                           'filters', f*randn(3,3,16,64, 'single'), ...
                           'biases', zeros(1, 64, 'single'), ...
                           'stride', 2, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'deconv', ...
                           'filters', f*randn(7,7,3,16, 'single'),...
                           'biases', zeros(1,16,'single'), ...
                           'stride', 2, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'euclid') ;


% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

[net, info] = cnn_train_scnseg(net, imdb, @getBatch, ...
    opts.train) ;

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(:,:,:,batch) ;
