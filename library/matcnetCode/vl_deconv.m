function [Y, dzdw1, dzdw2] = vl_deconv(X, F, B, FWstride, DZDY)
% forw original
% (res(i).x, l.filters, l.biases, 'pad', l.pad, 'stride', l.stride) ;
% back original
% [res(i).dzdx, res(i).dzdw{1}, res(i).dzdw{2}] = ...
%             vl_nnconv(res(i).x, l.filters, l.biases, ...
%                       res(i+1).dzdx, ...
%                       'pad', l.pad, 'stride', l.stride) ;
persistent dzdw01 dzdw02

    if false % some assertion
        error('reshape size does not match')
    end
    if nargin < 5
        % forward pass
        % but calc and store dzdw
        [Y, dzdw01,dzdw02] = vl_nnconv(gpuArray(...
            ones([(size(X,1)*FWstride+size(F,1)-1) ...
            (size(X,2)*FWstride+size(F,2)-1) size(F,3) size(X,4)],...
            'single')), gpuArray(F), gpuArray(B), X,'stride', FWstride);
    else
        % backward pass
        Y = vl_nnconv(DZDY, gpuArray(F), gpuArray(B),'stride', FWstride);
        dzdw1 = dzdw01./numel(DZDY);
        dzdw2 = dzdw02./numel(DZDY);
    end
end