function [Y,dzdP] = vl_f_batchNorm(X, params, dzdy)
% Implementation of the Batch-Normalization presented in:
% Ioffe/Szegedy, 2015, arXiv, 'Batch Normalization: Accelerating Deep
% Network Training by Reducing Internal Covariate Shift'

m = size(X,4); % batchsize
gamma = params(1);
beta= params(2);
varConvBool = params(3);
dzdP = [];

muBN        = mean(X,4);
Xmeanfree   = bsxfun(@minus,X,muBN);
varBN       = (1/m)*sum((Xmeanfree).^2,4);
% varConvBool encodes true if the convolutional variance is to be taken
if varConvBool
varBN = sum(sum(varBN,2),1);
end
% forward calculation: mean, var (per channel / per pixel), gamma, beta
Xnorm = bsxfun(@rdivide,Xmeanfree,varBN+eps);

if nargin <= 2
    Y = bsxfun(@plus,bsxfun(@times,gamma,Xnorm),beta);
else
    % backward propagation: using derivative formulas from the paper
    dzdXn       = dzdy * gamma;
    dzdVarBN    = sum(bsxfun(@times,dzdXn*(X-muBN)*(-.5),(varBN+eps).^(-1.5)),4);
    dzdMuBN     = sum(bsxfun(@rdivide,-dzdXn,sqrt(varBN+eps)),4) + bsxfun(@times,dzdVarBN,sum(-2*(X-muBN),4)/m);
    dzdX        = bsxfun(@rdivide,dzdXn,sqrt(varBN+eps)) + bsxfun(@times,dzdVarBN,2*(X-muBN)/m) + dzdMuBN/m;
    Y = dzdX;
    dzdGamma    = sum(dzdy.*Xnorm,4);
    dzdBeta     = sum(dzdy,4);
    if varConvBool
    dzdGamma    = sum(sum(dzdGamma,2),1);
    dzdBeta     = sum(sum(dzdBeta,2),1);
    end
    dzdP{1} = dzdGamma;
    dzdP{2} = dzdBeta;
end

end