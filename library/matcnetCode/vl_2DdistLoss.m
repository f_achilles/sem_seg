function Y = vl_2DdistLoss(X,c,dzdy)
sz_ = [size(X,1) size(X,2) size(X,3) size(X,4)];
numPt = sz_(3)/2;
res = X-c;
invalidJoints = isnan(res);
res(invalidJoints)=0;
if all(sz_(1:2) == [1 1])
    dist = sqrt(...
        res(:,:,1:numPt,        :).^2 + ...
        res(:,:,(numPt+1):end,  :).^2   ...
        );
else
    error('2DdistLoss is used with tensor instead of coordinate-vector!');
end

% unlabelled joints are NaN, do not backproject an error here
% dist(isnan(dist)) = 0;

if nargin < 3
    Y = sum(dist,3);
else
    Y = dzdy * res./(cat(3,dist,dist)+eps);
end

Y = bsxfun(@rdivide,Y,(sum(~invalidJoints,3)/2)+eps);
end