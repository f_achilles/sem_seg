function [valueOutput,binaryOutput] = majorityFilter(inputMat, valuesRowVec)
binaryOutput=inputMat*0;
valueIdx = 1;
% To get rid of 0s in the data, only assign 0 if it is the only content.
zeroPositions = valuesRowVec(1)==inputMat(:);
if all(zeroPositions)
    binaryOutput(1)=1;
else
    [~,valueIdx]=max(sum(bsxfun(@eq, inputMat(~zeroPositions), valuesRowVec)));
    positionIdx=find(inputMat(:)==valuesRowVec(valueIdx),1,'first');
    binaryOutput(positionIdx)=1;
end
valueOutput = valuesRowVec(valueIdx);
end