inputsize = size(depthContainer,3);
outputsize = 40;
batchsize = 10;
offs = ceil(inputsize/(2*outputsize));
indexvec = ones(inputsize,1);
outInd = 1;
seizurePartFrameIndices = zeros(1,outputsize);

% the selection scheme for maximally different frames should approximate
% the optimal solution (combination 40 out of 1800 images -> too many
% possibilities!
%
% easy to implement method:
% search the input in randomized batches (e.g. 10), compute a value
% for each batch-element if it was added to the selection
% choose the element that results in the highest value after checking if it
% is already part of the selection.
% nice.
currentFrameInd = randi(inputsize,1);
seizurePartFrameIndices(1) = currentFrameInd;
while nnz(seizurePartFrameIndices)~=outputsize
    tic
    randomIdx = randi(inputsize,batchsize);
    variances = zeros(1,batchsize);
    validBatchElementAvailable = false;
    for i = 1:batchsize
        if ~any(randomIdx(i)==seizurePartFrameIndices)
            variances(i) = sum(sum(var(cat(3,double(depthContainer(:,:,seizurePartFrameIndices(seizurePartFrameIndices~=0))),double(depthContainer(:,:,randomIdx(i)))),0,3)));
            validBatchElementAvailable = true;
        end
    end
    if validBatchElementAvailable
        outInd = outInd+1;
        [~,batchIdx]=max(variances);
        currentFrameInd = randomIdx(batchIdx);
    end
    seizurePartFrameIndices(outInd) = currentFrameInd;
%     seizurePartFrameIndices(outInd) = gather(currentFrameInd);
%     indexvec((max(currentFrameInd-offs,1)):min(currentFrameInd+offs,size(depthContainer,3)))=0;
%     diffs = bsxfun(@minus,depthContainer,depthContainer(:,:,currentFrameInd));
%     diffsvec = squeeze(sum(sum(diffs.^2)));
%     [~,currentFrameInd] = max(diffsvec.*indexvec);
    disp(['frame ' num2str(outInd) ' computed.'])
    toc
end
seizurePartFrameIndices = sort(seizurePartFrameIndices);
outputContainer = depthContainer(:,:,seizurePartFrameIndices);

% store corresponding frame numbers
seizurePart = frameLow:frameHigh;
originalFrameNumbers = seizurePart(seizurePartFrameIndices);
timestamps = timestampContainer(seizurePart(seizurePartFrameIndices));
