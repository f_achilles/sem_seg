function [net, info] = cnn_scnsegLvl2(imdb)
% cnn_scnseg
% Test architecture for semantic segmentation on NYU2 dataset

% run(fullfile(fileparts(mfilename('fullpath')),'..','matlab','vl_setupnn.m')) ;

opts.dataDir = fullfile('..\data') ;
opts.expDir = fullfile('..\data\depthNet\bigNet') ;
opts.imdbPath = fullfile(opts.expDir, 'NYU2scale10TrainingAllLabels4ClassesExp.mat');
opts.train.batchSize = 3;
opts.train.numEpochs = 100 ;
opts.train.continue = true ;
opts.train.useGpu = true ;
opts.train.learningRate = 0.001 ;
opts.train.expDir = opts.expDir ;
opts.continue = true;
% opts = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

if exist(opts.imdbPath, 'file') && ~exist('imdb','var')
    imdb = load(opts.imdbPath) ;
    % convert struct data to 4D matrices format
    imdb = convertTo4dStruct(imdb.imdb);
    % pad GT data with 2 columns to match the CNN output
    imdb.images.labels = padarray(imdb.images.labels,[0 1 0 0],0,'both');
elseif ~exist('imdb','var')
    error('No training data found!')
end

% Define a network that does not change the input size
f=1/100 ;
net.layers = {} ;
% input size 240x320
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(7,7,1,16, 'single'), ...
                           'biases', zeros(1, 16, 'single'), ...
                           'stride', 1, ...
                           'pad', 3*ones(1,4)) ;

net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'normalize', ...
                           'param', [5 1 0.0001/5 0.75]) ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(5,5,16,64, 'single'),...
                           'biases', zeros(1,64,'single'), ...
                           'stride', 1, ...
                           'pad', 2*ones(1,4)) ;

net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'normalize', ...
                           'param', [5 1 0.0001/5 0.75]) ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(5,5,64,16, 'single'),...
                           'biases', zeros(1,16,'single'), ...
                           'stride', 1, ...
                           'pad', 2*ones(1,4)) ;

net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'normalize', ...
                           'param', [5 1 0.0001/5 0.75]) ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(3,3,16,4, 'single'),...
                           'biases', zeros(1,4,'single'), ...
                           'stride', 1, ...
                           'pad', 1*ones(1,4)) ;

net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'normalize', ...
                           'param', [5 1 0.0001/5 0.75]) ;

net.layers{end+1} = struct('type', 'softmaxloss') ;


% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

% randomIndices = randperm(numel(imdb.images.set));
% imdb.images.set(randomIndices(1:(ceil(numel(randomIndices)/10)))) = 2;
% Take the mean out
imdb.images.data = bsxfun(@minus, imdb.images.data, mean(imdb.images.data(:,:,:,imdb.images.set==1),4)) ;
imdb.images.labels = single(imdb.images.labels);

[net, info] = cnn_train_scnseg(net, imdb, @getBatch, ...
    opts.train) ;

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch) ;
% labels = imdb.images.labels(1,batch) ;
labels = imdb.images.labels(:,:,1,batch) ;

% --------------------------------------------------------------------
function imdb = getMnistImdb(opts)
% --------------------------------------------------------------------
files = {'train-images-idx3-ubyte', ...
         'train-labels-idx1-ubyte', ...
         't10k-images-idx3-ubyte', ...
         't10k-labels-idx1-ubyte'} ;

mkdir(opts.dataDir) ;
for i=1:4
  if ~exist(fullfile(opts.dataDir, files{i}), 'file')
    url = sprintf('http://yann.lecun.com/exdb/mnist/%s.gz',files{i}) ;
    fprintf('downloading %s\n', url) ;
    gunzip(url, opts.dataDir) ;
  end
end

f=fopen(fullfile(opts.dataDir, 'train-images-idx3-ubyte'),'r') ;
x1=fread(f,inf,'uint8');
fclose(f) ;
x1=permute(reshape(x1(17:end),28,28,60e3),[2 1 3]) ;

f=fopen(fullfile(opts.dataDir, 't10k-images-idx3-ubyte'),'r') ;
x2=fread(f,inf,'uint8');
fclose(f) ;
x2=permute(reshape(x2(17:end),28,28,10e3),[2 1 3]) ;

f=fopen(fullfile(opts.dataDir, 'train-labels-idx1-ubyte'),'r') ;
y1=fread(f,inf,'uint8');
fclose(f) ;
y1=double(y1(9:end)')+1 ;

f=fopen(fullfile(opts.dataDir, 't10k-labels-idx1-ubyte'),'r') ;
y2=fread(f,inf,'uint8');
fclose(f) ;
y2=double(y2(9:end)')+1 ;

imdb.images.data = single(reshape(cat(3, x1, x2),28,28,1,[])) ;
imdb.images.labels = cat(2, y1, y2) ;
imdb.images.set = [ones(1,numel(y1)) 3*ones(1,numel(y2))] ;
imdb.meta.sets = {'train', 'val', 'test'} ;
imdb.meta.classes = arrayfun(@(x)sprintf('%d',x),0:9,'uniformoutput',false) ;
