function varargout = playKDPT(filename, verbose, rotate)
if ~exist('verbose','var') || isempty(verbose)
    verbose = false;
end
if ~exist('rotate','var') || isempty(rotate)
    rotate = false;
end
output = 0;
if nargout > 0
    depthContainer = zeros(424,512,30*60,'uint16');
    timestampContainer = zeros(1,30*60);
    output = 1;
end
depthFileHandle = fopen(filename);
bitformat = '*uint16';

mainHeader=textscan(depthFileHandle,'%s\n',2);
disp(mainHeader{1})

% read initial frame
frameNr = 1;
timestamp = fread(depthFileHandle,18,'uint8');
disp(['time: ' char(timestamp(1:end-1)') ', frame: ' num2str(frameNr)])
% KV2 has 512�424 depth resolution
tmp1 = fread(depthFileHandle,[512 424],bitformat);
tmp1(tmp1>4500 | tmp1<500) = 0;
tmp1 = rot90(tmp1,-1);
if output
    depthContainer(:,:,1) = tmp1;
    timestampContainer(1) = str2double(char(timestamp(1:end-1)'));
end
if verbose
colormap hot
hD=imagesc(tmp1,[500 4500]);
axis image
title('KinectV2 depth video')
hT=text(10,40,{['time: ' char(timestamp(1:end-1)') ','],['frame: ' num2str(frameNr)]},'fontsize',20,'color','blue');
end

% read all frames until EOF and update figure
while 1
timestamp = fread(depthFileHandle,19,'uint8');
if isempty(timestamp)
    break
end
frameNr = frameNr+1;
disp(['time: ' char(timestamp(2:end-1)') ', frame: ' num2str(frameNr)])
% KV2 has 512�424 depth resolution
tmp1 = fread(depthFileHandle,[512 424],bitformat);
tmp1(tmp1>4500 | tmp1<500) = 0;
tmp1 = rot90(tmp1,-1);
if verbose
set(hD,'CDATA',tmp1);
end
if output
    depthContainer(:,:,frameNr) = tmp1;
    timestampContainer(frameNr) = str2double(char(timestamp(2:end-1)'));
end
if verbose
set(hT,'string',{['time: ' char(timestamp(2:end-1)') ','],['frame: ' num2str(frameNr)]});
drawnow;
end

end
fclose(depthFileHandle);

if output
depthContainer = depthContainer(:,:,1:frameNr);
timestampContainer = timestampContainer(1:frameNr);
varargout{1} = depthContainer;
varargout{2} = timestampContainer;
end

end