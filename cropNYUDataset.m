load('C:\Users\Felix\Downloads\nyu_depth_v2_labeled.mat')

H = 200;
W = 200;
N = size(depths,3);

% crop depths
% original depths are 480x640x1449
hOriginal = size(depths,1);
wOriginal = size(depths,2);
hCenter = floor(hOriginal/2);
wCenter = floor(wOriginal/2);
halfH = floor(H/2);
halfW = floor(W/2);

lowerBdH = 1+hCenter-halfH;
upperBdH = lowerBdH+H-1;

lowerBdW = 1+wCenter-halfW;
upperBdW = lowerBdW+W-1;

depths = depths(lowerBdH:upperBdH,lowerBdW:upperBdW,:);

% crop labels
% original labels are 480x640x1449
labels = labels(lowerBdH:upperBdH,lowerBdW:upperBdW,:);

% put depths in [H,W,1,N]-format for deepLearning lib
depths = reshape(depths,[H W 1 N]);

save('croppedDataNYU2','depths','labels')