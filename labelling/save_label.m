%%
clear all
load reg_annot2.mat
reg=1;
bw=zeros(424,512);
%class=1;
for j=1:40
 for c=1:6
  while size(position{j,c,reg},1)>0
    bwtemp = poly2mask(position{j,c,reg}(:,1),position{j,c,reg}(:,2),424,512);
    bw=bw+c*bwtemp;
    bw=((bw>c)*c)+bw.*(bw<=c);
    reg=reg+1;
  end
  reg=1;
 end
name=strcat('kirFrame',num2str(j),'_lab.png');
imwrite(uint8(bw),name);
end


