clear all
close all
srcFiles = dir('*.png');
rect=cell(1,size(srcFiles,1));
f = figure();

for i=1:size(srcFiles,1)
    I=imread(srcFiles(i).name);
    imshow(I)
    w=waitforbuttonpress;
    %If mouse is clicked, the face will be selected, if key is pressed go
    %to next frame
      ind=1;
      while(w==0)%while no key is pressed the rectangle is continuously defined
        rect{i}(ind,:)=getrect;
        for j=1:ind
           h=rectangle('Position',rect{i}(j,:));
        end
        w1=waitforbuttonpress;
        if (w1 == 1)
            ch=get(f,'currentcharacter')
            if(ch=='a')%if key 'a' is pressed then another person face is masked
               ind=ind+1;
               w=0;
            else
                w=1;% if other key is pressed, go to next frame
            end
        end
        if (w1==0)%if mouse is clicked, last rectangle is redefined
            imshow(I)
            if ind>1
                for j=1:ind-1
                    h=rectangle('Position',rect{i}(j,:));
                end
            end
        end
      end
end

%%
%writing masked images in a folder named 'mask'
[nrows, ncols] = cellfun(@size, rect);
for i=1:size(srcFiles,1)
    I=imread(srcFiles(i).name);
    for j=1:nrows(i)      
       mask=round(rect{i}(j,:));
       mask(mask<=0) = 1;
       I(mask(2):mask(2)+mask(4),mask(1):mask(1)+mask(3))=0;  
    end
    NewFile=strcat('mask/',srcFiles(i).name);
    imwrite(I,NewFile,'png');
end

    
