function [ position] = annotate1(positionmat)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
prompt = 'Enter prefix of filenames:';
str = input(prompt,'s');
if (~exist(positionmat,'file')) ||  isempty(positionmat)
    prompt = 'Enter number of images:';
    tot = input(prompt); 
    prompt = 'Enter number of labels:';
    lab= input(prompt);
    in=1;
    position=cell(tot,lab,20);
    num=tot;
else
    load(positionmat);
    prompt = 'Enter initial image to annotate:';
    in = input(prompt);
    prompt = 'Enter image for reference:';
    ref = input(prompt);
    prompt = 'Enter number of images to annotate:';
    num = input(prompt);
end

col_class={[1 1 0],[1 0 1],[0 1 0],[0 0 0],[1 0 0],[0 0 1]};


for i=in:in+num-1
K=0;
filename=strcat(str,num2str(i),'.png');
f = figure();
I=imread(filename);
imshow(I)
%K = waitforbuttonpress;
cnt = 1;
class=1;
while K==0
disp(strcat('Class ',num2str(class)));
if i==1
    h = impoly;
else
if size(position{i-1,class,cnt},1)>0 
    h = impoly(gca, position{i-1,class,cnt});
else
    h = impoly;
end
end
position{i,class,cnt} = wait(h); % double mouse klick
save(positionmat,'position');
if size(h,1)>0
 setColor(h,col_class{class});
end
%h.getPosition();
K = waitforbuttonpress;
if (K == 1)
            ch=get(f,'currentcharacter')
            if(ch=='a')%if key 'a' is pressed then next class is segmented
               class=class+1;
               cnt=0;
               K=0;
            else
                K=1;% if other key is pressed, go to next frame
            end
end
cnt = cnt + 1;

end
close(f)
end
save(positionmat,'position');
end

