% downsize the images to 240x320
% downsize the labels to 240x320 and 10 stages further down, until 24x32

% clc
% clear all

currFName = which('augmentNYUDatasetMultiscaleGT');
currPath = fileparts(currFName);

load([currPath '\..\data\nyu_depth_v2_labeled.mat'],'depths')
load([currPath '\..\data\NYU2labels4classes.mat'])

% image database struct
% im = image patch as H x W x numColors
% y  = label patch as H x W x 1
% set = element of [1;2] (1=train, 2=test)
% fr = index of the origin frame of patch 'im'
imdb = struct('im',[],'y',[],'set',[],'fr',[]);

N = size(labels,3);
hOriginal = size(labels,1);
wOriginal = size(labels,2);

hIn = hOriginal/2;
wIn = wOriginal/2;

numScales = 10;
byteSizeDepth = 4; % is encoded as float
byteSizeLabels = 2; % is encoded as uint16

hOut = linspace(hIn,hIn/numScales,numScales);
wOut = linspace(wIn,wIn/numScales,numScales);

majBlockFun = @(blockStr)majorityFilter(blockStr.data,0:4);

% % map human labels to structural classes using
% % get_object_to_structure_class_map function of
% % http://cs.nyu.edu/~silberman/code/indoor_scene_seg_sup.zip
% obj2strind = get_object_to_structure_class_map;
% for LL = 1:numel(names)
%     labels(labels==LL)=uint16(obj2strind(names{LL}));
% end
% 
% save(['C:\Projects\NYU2labels4classes'],'labels','-v7.3')


% split into 795 training and 645 test images
% indices for tran/test split are taken from
% http://horatio.cs.nyu.edu/mit/silberman/indoor_seg_sup/splits.mat
load([currPath '\..\NYU2TrainTestSplits.mat'])
trainingSize   = numel(trainNdxs);
validationSize = numel(testNdxs);

% rotate images about 'rotAngleSet' degrees
% this results in 1449 * #scales * numel(rotAngleSet) * 2
% ground truth-labeled images
rotAngleSet = [-8 -4 0 4 8];
jitterScaleRatioSet = [0.9 0.95 1 1.05 1.1];
scaleSet = 1:10;

for scale = scaleSet
    numTrainingImages = trainingSize*numel(rotAngleSet)*numel(jitterScaleRatioSet)*2;
    inputOutputPairSize = hIn*wIn*byteSizeDepth + hOut(scale)*wOut(scale)*byteSizeLabels;
    datasetSize       = numTrainingImages*inputOutputPairSize...
                      + ...
                      validationSize*inputOutputPairSize;
    disp(['Augmented dataset at scale ' num2str(scale) ' has ' num2str(numTrainingImages) ' training images and will require ' num2str(datasetSize) ' Bytes of RAM.'])
end
% scale = high: LOW output resolution (the scale-quotient is high)
% scale = low: HIGH output resolution
iterationCounter = 0;

tic
for scale = 1 %1:numScales
    for jitterScale = jitterScaleRatioSet
        % make new height and width divisible by 2 (input)
        hJitterIn = ceil(hIn * jitterScale);
        wJitterIn = ceil(wIn * jitterScale);
        if mod(hJitterIn,2) ~= 0
            hJitterIn = hJitterIn+1;
        end
        if mod(wJitterIn,2) ~= 0
            wJitterIn = wJitterIn+1;
        end
        if hJitterIn >= hIn
            padcropOperation = @(imTemp)imcrop(imTemp,[((wJitterIn-wIn)/2+1) ((hJitterIn-hIn)/2+1) wIn-1 hIn-1]);
        else
            padcropOperation = @(imTemp)padarray(imTemp,[(hIn-hJitterIn)/2, (wIn-wJitterIn)/2],0,'both');
        end
        % make new height and width divisible by 2 (output)
        hJitterOut = hJitterIn;
        wJitterOut = wJitterIn;
%         hJitterOut = ceil(hOut(scale) * jitterScale);
%         wJitterOut = ceil(wOut(scale) * jitterScale);
        if mod(hJitterOut,2) ~= 0
            hJitterOut = hJitterOut+1;
        end
        if mod(wJitterOut,2) ~= 0
            wJitterOut = wJitterOut+1;
        end
        if hJitterOut >= hOut(scale)
            padcropOperationOut = @(imTemp)imcrop(imTemp,[((wJitterOut-wOut(scale))/2+1) ((hJitterOut-hOut(scale))/2+1) wOut(scale)-1 hOut(scale)-1]);
        else
            padcropOperationOut = @(imTemp)padarray(imTemp,[(hOut(scale)-hJitterOut)/2, (wOut(scale)-wJitterOut)/2],0,'both');
        end
        for flipBool = [0 1]
            if flipBool
                flipOperation = @(x)fliplr(x);
            else
                flipOperation = @(x)x;
            end
            for rotAngle = rotAngleSet
                imdb = struct('im',[],'y',[],'set',[],'fr',[]);
                % create augmented training set
                for frameInd = 1:trainingSize
                    imTemp              = imresize(depths(:,:,trainNdxs(frameInd)),[hJitterIn, wJitterIn]);
                    imTemp              = imrotate(padcropOperation(imTemp),rotAngle,'bilinear','crop');
                    imdb(frameInd).im   = flipOperation(imTemp);
                    
                    outTemp             = padcropOperation(imresize(labels(:,:,trainNdxs(frameInd)),[hJitterOut, wJitterOut],'nearest'));
%                     outTemp             = blockproc(outTemp,[floor(hIn/hOut(scale)) floor(wIn/wOut(scale))],majBlockFun);
                    outTemp             = imrotate(outTemp,rotAngle,'nearest','crop');
                    imdb(frameInd).y    = flipOperation(outTemp);
                    imdb(frameInd).set  = 1;
                    imdb(frameInd).fr   = trainNdxs(frameInd);
                end
                % save set
                save(['C:\Projects\NYU2rot' num2str(rotAngle) 'GTscale' num2str(scale) 'JitterScale' num2str(100*jitterScale) 'Flip' num2str(flipBool) 'Training.mat'],'imdb')
                iterationCounter = iterationCounter+1;
                disp([num2str(iterationCounter) ' of 50 augmented sets generated...'])
            end
        end
    end
end
toc % takes about 9 Minutes per scale
% clear all
disp('You made it! Dataset stored on disk, RAM can breathe again.')

%% concatenation of the rotated/mirrored data into one file
% clear all
jitterScaleRatioSet = [0.9 0.95 1 1.05 1.1];
rotAngleSet = [-8 -4 0 4 8];
for scale = 1 %1:numScales
iterationCounter = 0;
for set = {'Training'}
    setString = set{1};
    for jitterScale = jitterScaleRatioSet
    for flipBool = [0 1]
    for rotAngle = rotAngleSet
        load(['C:\Projects\NYU2rot' num2str(rotAngle) 'GTscale' num2str(scale) 'JitterScale' num2str(100*jitterScale) 'Flip' num2str(flipBool) setString '.mat'])
        if iterationCounter == 0
            assignin('base','imdbAll',imdb);
        else
            imdbAll = cat(2,imdbAll,imdb);
        end
        iterationCounter = iterationCounter+1;
    end
    end
    end
    clear imdb
    assignin('base','imdb',imdbAll);
    clear imdbAll
    save(['C:\Projects\ImageSegmentation\data\betterLabels\NYU2scale' num2str(scale) setString 'All'],'imdb','-v7.3')
end
disp(['scale ' num2str(scale) ' done!'])
end

%% save downsampled validation data

scale = 10;
imdbVal = struct('im',[],'y',[],'set',[],'fr',[]);
for frameInd = 1:validationSize
        imdbVal.images.data(:,:,1,frameInd)     = imresize(depths(:,:,testNdxs(frameInd)),[hIn, wIn]);
        imdbVal.images.labels(:,:,1,frameInd)   = blockproc(labels(:,:,testNdxs(frameInd)),[floor(hOriginal/hOut(scale)) floor(wOriginal/wOut(scale))],majBlockFun);
        imdbVal.images.set(frameInd)            = 2;
        imdbVal.images.origIdx(frameInd)        = testNdxs(frameInd);
end
save(['C:\Projects\ImageSegmentation\data\betterLabels\NYU2scale' num2str(scale) 'Validation'],'imdbVal','-v7.3')
